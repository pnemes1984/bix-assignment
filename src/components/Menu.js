import React, { Component } from 'react';
import './Menu.css';

class Menu extends Component{
    render(){
        return(
            <div className="menu__global">
                <div className="menu__line">
                    <span 
                        className={
                            this.props.selected === 'create' ? 
                                `menu__option` :
                                `menu__option menu__option--hover` 
                        } 
                        id="menu__option--create"
                        onClick = {this.props.choose}
                    >
                        add a new contact
                    </span>
                    
                    <span 
                        className={
                            this.props.selected === 'load' ?
                                `menu__option` :
                                `menu__option menu__option--hover`
                        } 
                        id="menu__option--load"
                        onClick = {this.props.choose}
                    >
                        browse contacts
                    </span>
                </div>
                <div 
                    id="menu__cursor" 
                    className = {
                        `menu__cursor-${this.props.selected}`
                    }>
                </div>
            </div>
        );
    }
}

export default Menu;