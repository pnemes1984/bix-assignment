import React, { Component } from 'react';
import './MainScreen.css';
import LoadIcon from './LoadIcon';

class MainScreen extends Component{
    render(){
        return(
            <div className="mainScreen__container">
                <div className={`mainScreen__ribbon mainScreen__ribbon--${this.props.selected}`}>
                    <div className="mainScreen__option" id="mainScreen__none">
                        choose an option from the menu
                    </div>

                    <div className="mainScreen__option" id="mainScreen__create">
                        <span className="mainScreen__title">add a new contact</span>
                        <form className="form">
                            <span className="input__label">first name</span>
                            <input className="input" type="text" id="form__firstname" placeholder="first name"></input>
                            <span className="input__label">last name</span>
                            <input className="input" type="text" id="form__lastname" placeholder="last name"></input>
                            <span className="input__label">e-mail address</span>
                            <input className="input" type="email" id="form__email" placeholder="e-mail address"></input>
                            <div className="button button--create" onClick={this.props.makeApiCall} >Create contact</div>
                        </form>
                    </div>

                    <div className="mainScreen__option" id="mainScreen__get">
                        <span className="mainScreen__title">browse contacts</span>
                        <form className="form form--load">
                            <input className="input input--margin" type="text" id="form__search" placeholder="what are you looking for?"></input>
                            <div className="button button--load" onClick={this.props.makeApiCall}>filter</div>
                        </form>
                        <div id="mainScreen__resultBox">
                        {
                            this.props.isLoading ? <LoadIcon /> : null
                        }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default MainScreen;
