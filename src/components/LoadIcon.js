import React, { Component } from 'react';
import './LoadIcon.css';

class LoadIcon extends Component {
    render(){
        return(
            <div className="load-container">
                <span id="load-text">Loading results</span>
                <div id="load-icon">
                    <div id="load-cursor"></div>
                </div>
            </div>
        );
    }
}

export default LoadIcon;