import React, { Component } from 'react';
import './App.css';
import Menu from './components/Menu';
import MainScreen from './components/MainScreen';

class App extends Component {
  constructor(){
    super();
    this.state = {
      selected: 'none',
      first_name: '',
      last_name: '',
      email: '',
      search: '',
      loading: false
    }
  }

  render() {
    this.choose = (event) => {
      this.setState({
        selected: event.target.id.split('--')[1]
      });
    }

    this.makeApiCall = () => {
        if(this.state.selected === 'create'){
          const inputs = ['firstname', 'lastname', 'email'];

          inputs.forEach((current) => {
            this.setState({[current]: document.querySelector(`#form__${current}`).value});
            document.querySelector(`#form__${current}`).value='';
          });

          fetch('https://jackie12b7514.api-us1.com/admin/api.php?api_action=contact_add', {
            method: 'put',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
              api_action: 'contact_add',
              api_key: 'a82f174bf50b589274d8e85e138be070311f6acf7f74a9188e920d9222b0c33a6e16f407',
              email: this.state.email,
              first_name: this.state.first_name,
              last_name: this.state.last_name
            })
          });

        } else if (this.state.selected === 'load'){
          this.setState({search: document.querySelector('#form__search').value});
          document.querySelector('#form__search').value='';

          fetch('https://jackie12b7514.api-us1.com/admin/api.php?api_action=contact_list&api_key=a82f174bf50b589274d8e85e138be070311f6acf7f74a9188e920d9222b0c33a6e16f407&ids=all', {
            method: 'get',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          })
          .then(response => response.json())
          .then(data => document.querySelector('#mainScreen__resultBox').insertAdjacentHTML('beforeend', data.filter((cur) => {
            cur.includes(this.state.search)
            document.querySelector('#mainScreen__resultBox').setAttribute('isLoading', '');
          })))

          while(document.querySelector('#mainScreen__resultBox').firstChild){
            document.querySelector('#mainScreen__resultBox').removeChild(document.querySelector('#mainScreen__resultBox').firstChild);
          }
          this.setState({
            loading: true
          });
          //document.querySelector('#mainScreen__resultBox').setAttribute('isloading', 'yes');

        }
    }
    return (
      <div className="container">
        <div className="sideburn sideburn-left"></div>
        <div className="sideburn sideburn-right"></div>
        <header className="titlebox">
          home assignment for BIX
        </header>

        <Menu selected = {this.state.selected} choose = {this.choose.bind(this)} />
        <MainScreen selected = {this.state.selected} makeApiCall = {this.makeApiCall.bind(this)} isLoading = {this.state.loading}/>

        <footer className="titlebox">
          <div className="footer-textbox">
            <span>The application was made by Nemes Péter</span> <span>|</span><a href="mailto:pnemes1984@gmail.com">contact</a>
          </div>
        </footer>
      </div>
    );
  }
}

export default App;
